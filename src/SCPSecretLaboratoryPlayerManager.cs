﻿using uMod.Auth;
using uMod.Common;

namespace uMod.Game.SCPSecretLaboratory
{
    /// <summary>
    /// Represents a SCPSecretLaboratory player manager
    /// </summary>
    public class SCPSecretLaboratoryPlayerManager : PlayerManager<SCPSecretLaboratoryPlayer>
    {
        /// <summary>
        /// Create a new instance of the SCPSecretLaboratoryPlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public SCPSecretLaboratoryPlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }
    }
}
