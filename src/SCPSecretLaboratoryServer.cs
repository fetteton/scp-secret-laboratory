using GameCore;
using System;
using System.Globalization;
using System.Net;
using uMod.Common;
using uMod.Text;
using WebResponse = uMod.Common.Web.WebResponse;

namespace uMod.Game.SCPSecretLaboratory
{
    /// <summary>
    /// Represents the server hosting the game instance
    /// </summary>
    public class SCPSecretLaboratoryServer : IServer
    {
        /// <summary>
        /// Gets or sets the player manager
        /// </summary>
        public IPlayerManager PlayerManager { get; set; }

        #region Server Information

        /// <summary>
        /// Gets/sets the public-facing name of the server
        /// </summary>
        public string Name
        {
            get => ServerConsole._serverName;
            set => ServerConsole._serverName = value;
        }

        private static IPAddress address;
        private static IPAddress localAddress;

        /// <summary>
        /// Gets the public-facing IP address of the server, if known
        /// </summary>
        public IPAddress Address
        {
            get
            {
                try
                {
                    if (address == null || !Utility.ValidateIPv4(address.ToString()))
                    {
                        if (Utility.ValidateIPv4(ServerConsole.Ip) && !Utility.IsLocalIP(ServerConsole.Ip))
                        {
                            IPAddress.TryParse(ServerConsole.Ip, out address);
                            Interface.uMod.LogInfo($"IP address from server: {address}"); // TODO: Localization
                        }
                        else
                        {
                            Web.Client webClient = new Web.Client();
                            webClient.Get("https://api.ipify.org")
                               .Done(delegate (WebResponse response)
                               {
                                   if (response.StatusCode == 200)
                                   {
                                       IPAddress.TryParse(response.ReadAsString(), out address);
                                       Interface.uMod.LogInfo($"IP address from external API: {address}"); // TODO: Localization
                                   }
                               });
                        }
                    }

                    return address;
                }
                catch (Exception ex)
                {
                    Interface.uMod.LogWarning("Couldn't get server's public IP address", ex); // TODO: Localization
                    return IPAddress.Any;
                }
            }
        }

        /// <summary>
        /// Gets the local IP address of the server, if known
        /// </summary>
        public IPAddress LocalAddress
        {
            get
            {
                try
                {
                    return localAddress ?? (localAddress = Utility.GetLocalIP());
                }
                catch
                {
                    return IPAddress.Any;
                }
            }
        }

        /// <summary>
        /// Gets the public-facing network port of the server, if known
        /// </summary>
        public ushort Port => (ushort)ServerConsole.Port;

        /// <summary>
        /// Gets the version or build number of the server
        /// </summary>
        public string Version => GameCore.Version.VersionString;

        /// <summary>
        /// Gets the network protocol version of the server
        /// </summary>
        public string Protocol => Version; // TODO: Implement if possible

        /// <summary>
        /// Gets the language set by the server
        /// </summary>
        public CultureInfo Language => CultureInfo.InstalledUICulture;

        /// <summary>
        /// Gets the total of players currently on the server
        /// </summary>
        public int Players => ServerConsole.PlayersAmount;

        /// <summary>
        /// Gets/sets the maximum players allowed on the server
        /// </summary>
        public int MaxPlayers
        {
            get => CustomNetworkManager.slots;
            set => CustomNetworkManager.slots = value;
        }

        /// <summary>
        /// Gets/sets the current in-game time on the server
        /// </summary>
        public DateTime Time
        {
            get => new DateTime(RoundStart.RoundLenght.Ticks);
            set => throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Gets the current or average frame rate of the server
        /// </summary>
        public int FrameRate
        {
            get => ServerStatic._serverTickrate;
        }

        /// <summary>
        /// Gets/sets the target frame rate for the server
        /// </summary>
        public int TargetFrameRate
        {
            get => UnityEngine.Application.targetFrameRate;
            set
            {
                ServerStatic._serverTickrate = (short)value;
                UnityEngine.Application.targetFrameRate = value;
            }
        }

        /// <summary>
        /// Gets information on the currently loaded save file
        /// </summary>
        ISaveInfo IServer.SaveInfo => null; // TODO: Implement if possible

        #endregion Server Information

        #region Server Administration

        /// <summary>
        /// Saves the server and any related information
        /// </summary>
        public void Save()
        {
            // TODO: Implement if possible
        }

        /// <summary>
        /// Shuts down the server, with optional saving and delay
        /// </summary>
        public void Shutdown(bool save = true, int delay = 0)
        {
            // TODO: Implement optional save and delay
            UnityEngine.Application.Quit();
        }

        #endregion Server Administration

        #region Player Administration

        /// <summary>
        /// Gets if player is banned on userid or ip
        /// </summary>
        /// <param name="playerId"></param>
        public BanHandler.BanType BanType(string playerId) => playerId.Contains("@") ? BanHandler.BanType.UserId : BanHandler.BanType.IP;

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string playerId, string reason = "", TimeSpan duration = default)
        {
            if (!IsBanned(playerId))
            {
                long issuanceTime = TimeBehaviour.CurrentTimestamp();
                long banExpirationTime = TimeBehaviour.GetBanExpirationTime((uint)duration.Seconds);

                BanHandler.IssueBan(new BanDetails
                {
                    OriginalName = "Unknown Name",
                    Id = playerId,
                    IssuanceTime = issuanceTime,
                    Expires = banExpirationTime,
                    Reason = reason,
                    Issuer = "Dedicated Server"
                }, BanType(playerId));

                /* // TODO: See about implementing universal IP ban options
                BanHandler.IssueBan(new BanDetails
                {
                    OriginalName = player.nicknameSync.MyNick,
                    Id = player.characterClassManager.connectionToClient.address,
                    IssuanceTime = issuanceTime,
                    Expires = banExpirationTime,
                    Reason = reason,
                    Issuer = "Dedicated Server"
                }, BanHandler.BanType.IP);
                */
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        /// <param name="playerId"></param>
        public TimeSpan BanTimeRemaining(string playerId)
        {
            return IsBanned(playerId) ? new TimeSpan(BanHandler.GetBan(playerId, BanType(playerId)).Expires - TimeBehaviour.CurrentTimestamp()) : TimeSpan.Zero;
        }

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        /// <param name="playerId"></param>
        public bool IsBanned(string playerId) => BanHandler.GetBan(playerId, BanType(playerId)) != null;

        /// <summary>
        /// Unbans the player
        /// </summary>
        /// <param name="playerId"></param>
        public void Unban(string playerId)
        {
            if (IsBanned(playerId))
            {
                BanHandler.RemoveBan(playerId, BanType(playerId));
            }
        }

        #endregion Player Administration

        #region Chat and Commands

        /// <summary>
        /// Broadcasts the specified chat message to all players with the specified icon
        /// </summary>
        /// <param name="message"></param>
        /// <param name="id"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, string prefix, ulong id, params object[] args)
        {
            message = !string.IsNullOrEmpty(prefix) ? $"{prefix}: {message}" : message;
            message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), args) : Formatter.ToUnity(message);
            ReferenceHub.HostHub.gameObject.GetComponent<Broadcast>().RpcAddElement(message, 5, global::Broadcast.BroadcastFlags.Normal);
        }

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, string prefix, params object[] args) => Broadcast(message, prefix, 0uL, args);

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players with the specified icon
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="id"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, ulong id, params object[] args) => Broadcast(message, string.Empty, id, args);

        /// <summary>
        /// Broadcasts the specified chat message to all players
        /// </summary>
        /// <param name="message"></param>
        public void Broadcast(string message) => Broadcast(message, null);

        /// <summary>
        /// Runs the specified server command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command))
            {
                ConsoleColor color;
                ServerConsole.EnterCommand(command + string.Join(" ", args), out color);
            }
        }

        #endregion Chat and Commands
    }
}
