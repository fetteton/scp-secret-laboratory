﻿using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Game.SCPSecretLaboratory
{
    /// <summary>
    /// Responsible for loading the core plugin
    /// </summary>
    public class SCPSecretLaboratoryPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(SCPSecretLaboratory) };

        public SCPSecretLaboratoryPluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}
